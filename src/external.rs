use downcast::Any;
use ioc;

pub trait SubsystemBase: Any + Send + Sync {
    fn on_before_frame(&mut self){}
    fn on_after_frame(&mut self){}
    fn on_reset(&mut self){}
}

impl_downcast!(SubsystemBase);
downcast_methods!(SubsystemBase);

impl<T: SubsystemBase + ?Sized> SubsystemBase for Box<T> {
    fn on_before_frame(&mut self){ (**self).on_before_frame() }
    fn on_after_frame(&mut self){ (**self).on_after_frame() }
    fn on_reset(&mut self){ (**self).on_reset() }

}

pub type External<Key = String, SsysBase: ?Sized = SubsystemBase> = ioc::Container<Key, SsysBase>;
pub type ExternalBuilder<Key = String, SsysBase: ?Sized = SubsystemBase> = ioc::ContainerBuilder<Key, SsysBase>;

