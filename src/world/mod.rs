// ++++++++++++++++++++ common ++++++++++++++++++++

use super::bitset::BitSet;

use downcast::Any;

use std::fmt::Debug;
use std::hash::Hash;

pub type Index = u32;
pub type Generation = u32;

pub trait Id: Hash + Debug + Copy + Ord + Any + Send + Sync + Sized {
    type UserData;

    fn new_unchecked(idx: Index, gen: Generation, user_data: Self::UserData) -> Self;
    fn new(idx: Index, gen: Generation, user_data: Self::UserData) -> Self {
        assert!(idx <= Self::max_index());

        if gen > Self::max_generation() {
            Self::new_unchecked(idx, 0, user_data)
        } else {
            Self::new_unchecked(idx, gen, user_data)
        }
    }

    fn max_index() -> Index;
    fn max_generation() -> Generation;

    fn index(self) -> Index;
    fn generation(self) -> Generation;
    fn user_data(self) -> Self::UserData;

    fn decompose(self) -> (Index, Generation, Self::UserData) {
        (self.index(), self.generation(), self.user_data())
    }
}

pub trait Value: Clone + Any + Send + Sync + Sized {}
impl<T: Clone + Any + Send + Sync> Value for T {}

///TODO replace this by a tuple?
#[derive(Debug, Clone, Copy)]
pub struct Instantiation<X> {
    pub dest: X,
    pub src: X,
    pub overwrite: bool,
}

pub trait StoreBase: Any + Send + Sync {
    fn contains(&self, idx: Index) -> bool;

    ///TODO `deletions` should be `&[Index]`.
    fn delete(&mut self, deletions: &BitSet);
    fn clear(&mut self);
}

impl_downcast!(StoreBase);
downcast_methods!(StoreBase);

pub trait Store: StoreBase {
    type Value: Clone + Any + Send + Sync;

    fn get(&self, idx: Index) -> Option<&Self::Value>;
    fn get_mut(&mut self, idx: Index) -> Option<&mut Self::Value>;

    fn insert(&mut self, idx: Index, val: Self::Value) -> Option<Self::Value>;
    fn remove(&mut self, idx: Index) -> Option<Self::Value>;
}

pub trait Instantiate<Src: Store>: Store<Value = Src::Value> {
    fn instantiate_from(&mut self, src: &Src, insts: &[Instantiation<Index>]){
        for inst in insts {
            if !inst.overwrite && self.contains(inst.dest) {
                continue
            }

            //TODO when inst.overwrite and component already exists, we could use clone_from

            src.get(inst.src).map(|val| self.insert(inst.dest, val.clone()));
        }
    }
}

pub trait Blend<Dest: Store>: Store<Value = Dest::Value> {
    fn blend_to(&mut self, dest: &mut Dest);
}

// ++++++++++++++++++++ submodules ++++++++++++++++++++

pub mod ioc_ext;
pub mod entry;
pub mod head;
mod world;

pub use self::entry::Entry;
pub use self::head::Head;
#[doc(inline)]
pub use self::world::*;

