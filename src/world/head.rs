use super::super::bitset::{BitSet, BitSetLike};
use super::super::join;
use super::entry::Entry;
use super::{Index, Id, Instantiation};

use crossbeam::sync::SegQueue;
use vec_map::{self, VecMap};

use std::sync::atomic::{AtomicU32, Ordering};

// ++++++++++++++++++++ IdGenerator ++++++++++++++++++++

pub struct IdAllocator<ID> {
    highest_idx: AtomicU32,
    freed_ids: SegQueue<ID>,
}

impl<ID: Id> IdAllocator<ID> {
    fn new() -> Self {
        IdAllocator{
            highest_idx: AtomicU32::new(0),
            freed_ids: SegQueue::new(),
        }
    }

    fn allocate(&self, user_data: ID::UserData) -> ID {
        match self.freed_ids.try_pop() {
            Some(old_id) => {
                let (idx, prev_gen, _) = old_id.decompose();
                ID::new(idx, prev_gen + 1, user_data)
            }
            None => {
                let idx = self.highest_idx.fetch_add(1, Ordering::Relaxed); 
                ID::new(idx, 0, user_data)
            }
        }
    }
    fn push_freed(&self, id: ID){
        self.freed_ids.push(id);
    }
    fn reset(&mut self){
        self.highest_idx.store(0, Ordering::Relaxed);

        while let Some(_) = self.freed_ids.try_pop() { /* */ }
    }
}

// ++++++++++++++++++++ Head ++++++++++++++++++++

pub struct Head<ID> {
    active_ids: VecMap<ID>,
    id_alloc: IdAllocator<ID>,

    creation_buf: SegQueue<ID>,
    deletion_buf: SegQueue<ID>,
    instantiation_buf: SegQueue<Instantiation<Index>>,

    creations: BitSet,
    deletions: BitSet,
    instantiations: Vec<Instantiation<Index>>, 
}

impl<ID: Id> Head<ID> {
    pub(super) fn new() -> Self {
        Head{ 
            active_ids: VecMap::new(),
            id_alloc: IdAllocator::new(),

            creation_buf: SegQueue::new(),
            deletion_buf: SegQueue::new(),
            instantiation_buf: SegQueue::new(),

            creations: BitSet::new(),
            deletions: BitSet::new(),
            instantiations: Vec::new(),
        } 
    }

    pub fn entry(&self, id: ID) -> Option<Entry<ID>> {
        self.active_ids.get(id.index() as usize).map(|id2| {
            assert_eq!(id, *id2);
            Entry::new(id)
        })
    }

    pub fn create(&self, user_data: ID::UserData) -> Entry<ID> {
        let id = self.id_alloc.allocate(user_data);
        self.creation_buf.push(id);
        Entry::new(id)
    }
    ///TODO naming? `queue_deletion`? `mark_deletion`?
    pub fn delete<'a>(&self, ent: Entry<'a, ID>){
        self.deletion_buf.push(ent.id())
    }
    ///TODO naming? `queue_instantiation`?
    pub fn instantiate<'a>(&self, inst: Instantiation<Entry<'a, ID>>){
        self.instantiation_buf.push(Instantiation{
            dest: inst.dest.id().index(),
            src: inst.src.id().index(),
            overwrite: inst.overwrite,
        });
    }

    /// Entries created during the current frame.
    pub fn creations(&self) -> &BitSet { &self.creations }

    /// Entries marked for deletion during the current frame.
    pub fn deletions(&self) -> &BitSet { &self.deletions }

    /// Instantiations queued during the previous stage.
    pub(super) fn instantiations(&self) -> &[Instantiation<Index>] { &self.instantiations }

    pub(super) fn flush_stage(&mut self){
        // make creations public
        while let Some(id) = self.creation_buf.try_pop() {
            self.active_ids.insert(id.index() as usize, id);
            self.creations.insert(id.index());
        }

        // make deletions public
        while let Some(id) = self.deletion_buf.try_pop() {
            self.deletions.insert(id.index());
        }

        // clear instantiations & make new ones public
        self.instantiations.clear();
        while let Some(inst) = self.instantiation_buf.try_pop() {
            self.instantiations.push(inst);
        }
    }

    pub(super) fn flush_frame(&mut self){
        for idx in (&self.deletions).iter() {
            let id = self.active_ids.remove(idx as usize).unwrap();
            self.id_alloc.push_freed(id);
        }
        self.creations.clear();
        self.deletions.clear();
        self.instantiations.clear();
    }

    pub(super) fn clear(&mut self){
        self.active_ids.clear();
        self.id_alloc.reset();

        while let Some(_) = self.creation_buf.try_pop() { /* */ }
        while let Some(_) = self.deletion_buf.try_pop() { /* */ }
        while let Some(_) = self.instantiation_buf.try_pop() { /* */ }

        self.creations.clear();
        self.deletions.clear();
        self.instantiations.clear();
    }

    pub fn join<J: join::Join>(&self, join: J) -> JoinIter<ID, J> {
        JoinIter{ head: self, join_iter: join.iter() }
    }
    pub fn iter(&self) -> Iter<ID> {
        Iter{ active_ids: self.active_ids.iter() }
    }
}

pub struct JoinIter<'a, ID: 'a, J: join::Join + 'a> {
    head: &'a Head<ID>,
    join_iter: join::JoinIter<J>,
}

impl<'a, ID, J> Iterator for JoinIter<'a, ID, J>
    where ID: Id, J: join::Join
{
    type Item = (Entry<'a, ID>, J::Type);
    fn next(&mut self) -> Option<Self::Item> {
        //TODO warn if join-indices don't match entries (creations not yet flushed)
        //self.join_iter.next().map(|(idx, vals)| (Entry::new(self.head.active_ids[idx as usize]), vals))

        self.join_iter.next().and_then(|(idx, vals)| {
            self.head.active_ids.get(idx as usize).map(|&id| (Entry::new(id), vals))
        })
    }
}

#[derive(Clone)]
pub struct Iter<'a, ID: 'a>{
    active_ids: vec_map::Iter<'a, ID>
}

impl<'a, ID: Id> Iterator for Iter<'a, ID> {
    type Item = Entry<'a, ID>;
    fn next(&mut self) -> Option<Self::Item> {
        self.active_ids.next().map(|(_, id)| Entry::new(*id))
    }
}

