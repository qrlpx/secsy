use super::{Index, Id, StoreBase, Store};

use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

macro_rules! e {
    ($e:expr) => { $e }
}

// ++++++++++++++++++++ Entry ++++++++++++++++++++

///TODO: rename to `Entity`?
#[derive(Clone, Copy)]
pub struct Entry<'a, ID> {
    id: ID,
    _p: PhantomData<&'a ()>
}

impl<'a, ID: Id> Entry<'a, ID> {
    pub(super) fn new(id: ID) -> Self { Entry{ id: id, _p: PhantomData } }

    pub fn id(&self) -> ID { self.id }

    pub fn contains<Args: GetTuple>(&self, args: Args) -> bool {
        args.contains(self.id.index())
    }
    pub fn get<Args: GetTuple>(&self, args: Args) -> Option<Args::Refs> {
        args.get(self.id.index())   
    }
    pub fn get_opt<Args: GetTuple>(&self, args: Args) -> Args::RefOpts {
        args.get_opt(self.id.index())   
    }

    pub fn insert<Args: InsertTuple>(&self, args: Args, vals: Args::Vals) -> Args::ValOpts {
        args.insert(self.id.index(), vals)   
    }

    pub fn remove<Args: InsertTuple>(&self, args: Args) -> Option<Args::Vals> {
        args.remove(self.id.index())
    }
    pub fn remove_opt<Args: InsertTuple>(&self, args: Args) -> Args::ValOpts {
        args.remove_opt(self.id.index())
    }
}

// ++++++++++++++++++++ Get ++++++++++++++++++++

pub trait Get {
    type Ref;
    fn contains(self, idx: Index) -> bool;
    fn get(self, idx: Index) -> Option<Self::Ref>;
}

impl<'a, T> Get for &'a T 
    where T: Deref, T::Target: Store
{
    type Ref = &'a <T::Target as Store>::Value;
    fn contains(self, idx: Index) -> bool {
        (**self).contains(idx)
    }
    fn get(self, idx: Index) -> Option<Self::Ref> { 
        (**self).get(idx)
    }
}

impl<'a, T> Get for &'a mut T 
    where T: DerefMut, T::Target: Store
{
    type Ref = &'a mut <T::Target as Store>::Value;
    fn contains(self, idx: Index) -> bool {
        (**self).contains(idx)
    }
    fn get(mut self, idx: Index) -> Option<Self::Ref> {
        (**self).get_mut(idx)
    }
}

// ++++++++++++++++++++ GetTuple ++++++++++++++++++++

pub trait GetTuple {
    type Refs;
    type RefOpts;
    fn contains(self, idx: Index) -> bool;
    fn get(self, idx: Index) -> Option<Self::Refs>;
    fn get_opt(self, idx: Index) -> Self::RefOpts;
}

impl<A: Get> GetTuple for A {
    type Refs = A::Ref;
    type RefOpts = Option<A::Ref>;
    fn contains(self, idx: Index) -> bool { 
        Get::contains(self, idx) 
    }
    fn get(self, idx: Index) -> Option<Self::Refs> {
        Get::get(self, idx)
    }
    fn get_opt(self, idx: Index) -> Self::RefOpts {
        Get::get(self, idx)
    }
}

macro_rules! get_tuple_impls {
    ($(($($params:ident:$fields:tt),+))+) => {
        $(
            impl<$($params),+> GetTuple for ($($params),+,)
                where $($params: Get),+
            {
                type Refs = ($($params::Ref),+,);
                type RefOpts = ($(Option<$params::Ref>),+,);

                fn contains(self, idx: Index) -> bool {
                    $(if !e![self.$fields.contains(idx)] { return false })+
                    true
                }
                fn get(self, idx: Index) -> Option<Self::Refs> {
                    Some(($(
                        match e![self.$fields.get(idx)] {
                            Some(r) => r, None => return None
                        }
                    ),+,))
                }
                fn get_opt(self, idx: Index) -> Self::RefOpts {
                    ($(e![self.$fields.get(idx)]),+,)
                }
            }
        )+
    }
}

get_tuple_impls!{
    (A:0)
    (A:0, B:1)
    (A:0, B:1, C:2)
    (A:0, B:1, C:2, D:3)
    (A:0, B:1, C:2, D:3, E:4)
    (A:0, B:1, C:2, D:3, E:4, F:5)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8, K:9)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8, K:9, L:10)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8, K:9, L:10, M:11)
}

// ++++++++++++++++++++ Insert ++++++++++++++++++++

///TODO naming?
pub trait Insert {
    type Val;
    fn insert(self, idx: Index, val: Self::Val) -> Option<Self::Val>;
    fn remove(self, idx: Index) -> Option<Self::Val>;
}

impl<'a, T> Insert for &'a mut T 
    where T: DerefMut, T::Target: Store
{
    type Val = <T::Target as Store>::Value;
    fn insert(self, idx: Index, val: Self::Val) -> Option<Self::Val> {
        (**self).insert(idx, val)
    }
    fn remove(self, idx: Index) -> Option<Self::Val> {
        (**self).remove(idx)
    }
}

// ++++++++++++++++++++ InsertTuple ++++++++++++++++++++

///TODO naming?
pub trait InsertTuple {
    type Vals;
    type ValOpts;

    fn insert(self, idx: Index, vals: Self::Vals) -> Self::ValOpts;
    //TODO? fn replace(self, idx: Index, vals: Self::Vals) -> Option<Self::Vals>;

    fn remove(self, idx: Index) -> Option<Self::Vals>;
    fn remove_opt(self, idx: Index) -> Self::ValOpts;
}

impl<A: Insert> InsertTuple for A {
    type Vals = A::Val;
    type ValOpts = Option<A::Val>;

    fn insert(self, idx: Index, vals: Self::Vals) -> Self::ValOpts {
        Insert::insert(self, idx, vals)
    }

    fn remove(self, idx: Index) -> Option<Self::Vals> {
        Insert::remove(self, idx)
    }
    fn remove_opt(self, idx: Index) -> Self::ValOpts {
        Insert::remove(self, idx)
    }
}

macro_rules! insert_tuple_impls {
    ($(($($params:ident:$fields:tt),+))+) => {
        $(
            impl<$($params),+> InsertTuple for ($($params),+,)
                where $($params: Insert),+
            {
                type Vals = ($($params::Val),+,);
                type ValOpts = ($(Option<$params::Val>),+,);

                fn insert(self, idx: Index, vals: Self::Vals) -> Self::ValOpts {
                    ($(e![self.$fields.insert(idx, vals.$fields)]),+,)
                }

                fn remove(self, idx: Index) -> Option<Self::Vals> {
                    Some(($(
                        match e![self.$fields.remove(idx)] {
                            Some(r) => r, None => return None
                        }
                    ),+,))
                }
                fn remove_opt(self, idx: Index) -> Self::ValOpts {
                    ($(e![self.$fields.remove(idx)]),+,)
                }
            }
        )+
    }
}

insert_tuple_impls!{
    (A:0)
    (A:0, B:1)
    (A:0, B:1, C:2)
    (A:0, B:1, C:2, D:3)
    (A:0, B:1, C:2, D:3, E:4)
    (A:0, B:1, C:2, D:3, E:4, F:5)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8, K:9)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8, K:9, L:10)
    (A:0, B:1, C:2, D:3, E:4, F:5, G:6, H:7, J:8, K:9, L:10, M:11)
}


