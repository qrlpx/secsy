use super::head::Head;
use super::{Index, Id, Instantiation, StoreBase, Instantiate, Blend};
use super::ioc_ext;

use rayon::prelude::*;
use downcast::Downcast;
use ioc;

pub type State<Key, StoBase: ?Sized> = ioc::Container<Key, StoBase>;
pub type Diff<Key, StoBase: ?Sized> = ioc::Container<Key, StoBase>;

pub type InstantiationFn<StoBase: ?Sized> = Box<Fn(&mut StoBase, &StoBase, &[Instantiation<Index>]) + Send + Sync>;
pub type BlendFn<StoBase: ?Sized> = Box<Fn(&mut StoBase, &mut StoBase) + Send + Sync>;

// ++++++++++++++++++++ World ++++++++++++++++++++

pub struct World<ID, Key = String, StoBase: ?Sized = StoreBase> {
    head: Head<ID>,
    state: State<Key, StoBase>,
    diff: Diff<Key, StoBase>,
    inst_fns: Vec<(Key, InstantiationFn<StoBase>)>,
    blend_fns: Vec<(Key, BlendFn<StoBase>)>,
}

impl<ID, Key, StoBase: ?Sized> World<ID, Key, StoBase>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase
{
    pub fn head(&self) -> &Head<ID> { &self.head }
    pub fn state(&self) -> &State<Key, StoBase> { &self.state }
    pub fn diff(&self) -> &Diff<Key, StoBase> { &self.diff }

    /// Makes creations and deletions of the current - now previous - stage visible.
    pub fn flush_stage(&mut self){
        self.head.flush_stage();

        //TODO weight?
        self.inst_fns.par_iter().for_each(|&(ref key, ref inst_fn)| {
            let state_sto = self.state.write_service_base(key).unwrap();
            let mut diff_sto = self.diff.write_service_base(key).unwrap();
            inst_fn(&mut diff_sto, &state_sto, self.head.instantiations());
        });

    }
    pub fn flush_frame(&mut self){
        // Ensure stage was flushed. This should already have happened.
        // If we make flush_{stage, frame} non-pub, we can remove this.
        self.flush_stage();

        // Blend stores & remove deleted components from state.
        {
            self.blend_fns.par_iter().weight_max().for_each(|&(ref key, ref blend_fn)| {
                let mut state_sto = self.state.write_service_base(key).unwrap();
                let mut diff_sto = self.diff.write_service_base(key).unwrap();
                blend_fn(&mut diff_sto, &mut state_sto);
                state_sto.delete(self.head.deletions());
            });
        }

        self.head.flush_frame();
    }

    pub fn clear(&mut self){
        self.head.clear();
        
        // Clear stores.
        let stores: Vec<_> = self.state.services().iter().chain(self.diff.services()).collect();
        stores.into_par_iter().for_each(|(_, store)| {
            store.write().unwrap().clear();
        });
    }
}

// ++++++++++++++++++++ WorldBuilder ++++++++++++++++++++

pub struct WorldBuilder<ID, Key = String, StoBase: ?Sized = StoreBase> {
    head: Head<ID>,
    state: ioc::ContainerBuilder<Key, StoBase>,
    diff: ioc::ContainerBuilder<Key, StoBase>,
    inst_fns: Vec<(Key, InstantiationFn<StoBase>)>,
    blend_fns: Vec<(Key, BlendFn<StoBase>)>,
}

impl<ID, Key, StoBase: ?Sized> WorldBuilder<ID, Key, StoBase>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase
{
    pub fn new() -> Self {
        WorldBuilder{ 
            head: Head::new(),
            state: ioc::ContainerBuilder::new(), 
            diff: ioc::ContainerBuilder::new(), 
            inst_fns: Vec::new(), 
            blend_fns: Vec::new(),
        }
    }

    pub fn register_component(
        &mut self, 
        key: Key, 
        state: Box<StoBase>, 
        diff: Box<StoBase>,
        inst_fn: InstantiationFn<StoBase>,
        blend_fn: BlendFn<StoBase>,
    ) -> &mut Self {
        self.state.register_service(key.clone(), state);   
        self.diff.register_service(key.clone(), diff);   

        self.inst_fns.retain(|&(ref key2, _)| &key != key2);
        self.inst_fns.push((key.clone(), inst_fn));

        self.blend_fns.retain(|&(ref key2, _)| &key != key2);
        self.blend_fns.push((key, blend_fn));

        self
    }

    pub fn register<C>(
        &mut self, 
        state_impl: C::StateStoreImpl,
        diff_impl: C::DiffStoreImpl
    ) -> &mut Self
    where
        C: ioc_ext::Component<Key = Key>, 
        ioc_ext::StateStore<C>: Into<Box<StoBase>>,
        ioc_ext::DiffStore<C>: Into<Box<StoBase>>,
        StoBase: Downcast<ioc_ext::StateStore<C>> + Downcast<ioc_ext::DiffStore<C>>
    { 
        let inst_fn = |diff: &mut StoBase, state: &StoBase, insts: &[Instantiation<Index>]| {
            let state: &ioc_ext::StateStore<C> = state.downcast_ref().unwrap();
            let diff: &mut ioc_ext::DiffStore<C> = diff.downcast_mut().unwrap();
            diff.instantiate_from(state, insts)
        };
        let blend_fn = |diff: &mut StoBase, state: &mut StoBase| {
            let state: &mut ioc_ext::StateStore<C> = state.downcast_mut().unwrap();
            let diff: &mut ioc_ext::DiffStore<C> = diff.downcast_mut().unwrap();
            diff.blend_to(state)
        };
        self.register_component(
            C::key().clone(), 
            ioc_ext::StateStore::<C>::new(state_impl).into(),
            ioc_ext::DiffStore::<C>::new(diff_impl).into(),
            Box::new(inst_fn),
            Box::new(blend_fn),
        )
    }

    pub fn register_defaults<C>(&mut self) -> &mut Self
    where
        C: ioc_ext::Component<Key = Key>, 
        ioc_ext::StateStore<C>: Into<Box<StoBase>>,
        ioc_ext::DiffStore<C>: Into<Box<StoBase>>,
        C::StateStoreImpl: Default,
        C::DiffStoreImpl: Default,
        StoBase: Downcast<ioc_ext::StateStore<C>> + Downcast<ioc_ext::DiffStore<C>>
    { 
        self.register::<C>(Default::default(), Default::default())
    }

    pub fn build(self) -> World<ID, Key, StoBase> {
        World{ 
            head: self.head,
            state: self.state.build(), 
            diff: self.diff.build(), 
            inst_fns: self.inst_fns,
            blend_fns: self.blend_fns, 
        }
    }
}

impl<ID, Key, StoBase: ?Sized> Default for WorldBuilder<ID, Key, StoBase>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase
{
    fn default() -> Self { Self::new() }
}

