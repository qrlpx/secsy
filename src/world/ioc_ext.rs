use super::super::bitset::BitSet;
use super::super::join;
use super::{Index, Value, StoreBase, Store, Instantiate, Blend};
use ioc;

use std::ops::{Deref, DerefMut};

// ++++++++++++++++++++ reflection ++++++++++++++++++++

pub trait Component: Value {
    type Key: ioc::Key;
    fn key() -> &'static Self::Key;

    type StateStoreImpl: Store<Value = Self>;
    type DiffStoreImpl: Instantiate<Self::StateStoreImpl> + Blend<Self::StateStoreImpl>;
}

pub trait ComponentTuple {
    type StateStoreTuple;
    type DiffStoreTuple;
}

impl ComponentTuple for () {
    type StateStoreTuple = ();
    type DiffStoreTuple = ();
}

macro_rules! component_tuple_impls {
    ($(($($params:ident)+))+) => {
        $(
            impl<$($params),+> ComponentTuple for ($($params),+,) 
                where $($params: Component),+
            {
                type StateStoreTuple = ($(StateStore<$params>),+,);
                type DiffStoreTuple = ($(DiffStore<$params>),+,);
            }
        )+
    }
}

component_tuple_impls!{
    (A)
    (A B)
    (A B C)
    (A B C D)
    (A B C D E)
    (A B C D E F)
    (A B C D E F G)
    (A B C D E F G H)
    (A B C D E F G H I)
    (A B C D E F G H I J)
    (A B C D E F G H I J K)
    (A B C D E F G H I J K L)
}

pub type ReadState<T: ComponentTuple> = ioc::Read<T::StateStoreTuple>;
pub type WriteState<T: ComponentTuple> = ioc::Write<T::StateStoreTuple>;
pub type ReadDiff<T: ComponentTuple> = ioc::Read<T::DiffStoreTuple>;
pub type WriteDiff<T: ComponentTuple> = ioc::Write<T::DiffStoreTuple>;

// ++++++++++++++++++++ store wrappers ++++++++++++++++++++

///TODO remove this?
pub struct StateStore<C: Component> {
    impl_: C::StateStoreImpl
}

///TODO remove this?
pub struct DiffStore<C: Component> {
    impl_: C::DiffStoreImpl
}

impl<C: Component> Default for StateStore<C>
    where C::StateStoreImpl: Default
{
    fn default() -> Self { StateStore{ impl_: Default::default() } }
}

impl<C: Component> Default for DiffStore<C>
    where C::DiffStoreImpl: Default
{
    fn default() -> Self { DiffStore{ impl_: Default::default() } }
}

macro_rules! impl_wrapper {
    ($wrapper:ident, $impl_assoc:ident) => { 
        impl<C: Component> $wrapper<C> {
            pub fn new(impl_: C::$impl_assoc) -> Self { $wrapper{ impl_: impl_ } }
        }

        /*FIXME(negative bounds?)
        impl<C: Component> From<C::$impl_assoc> for $wrapper<C> {
            fn from(impl_: C::$impl_assoc) -> Self { $wrapper{ impl_: impl_ } }
        }
        */

        impl<C: Component> ioc::Service for $wrapper<C> {
            type Key = C::Key;
            fn key() -> &'static Self::Key { C::key() }
        }

        impl<C: Component> Into<Box<StoreBase>> for $wrapper<C> {
            fn into(self) -> Box<StoreBase> { Box::new(self) }
        }

        impl<C: Component> StoreBase for $wrapper<C> {
            fn contains(&self, idx: Index) -> bool { self.impl_.contains(idx) }

            fn delete(&mut self, deletions: &BitSet){ self.impl_.delete(deletions) }
            fn clear(&mut self){ self.impl_.clear() }
        }

        impl<C: Component> Store for $wrapper<C> {
            type Value = <C::$impl_assoc as Store>::Value;

            fn get(&self, idx: Index) -> Option<&Self::Value> { self.impl_.get(idx) }
            fn get_mut(&mut self, idx: Index) -> Option<&mut Self::Value> { self.impl_.get_mut(idx) }

            fn insert(&mut self, idx: Index, val: Self::Value) -> Option<Self::Value> { self.impl_.insert(idx, val) }

            fn remove(&mut self, idx: Index) -> Option<Self::Value> { self.impl_.remove(idx) }
        }

        impl<C: Component> Deref for $wrapper<C> {
            type Target = C::$impl_assoc;
            fn deref(&self) -> &Self::Target {
                &self.impl_
            }
        }

        impl<C: Component> DerefMut for $wrapper<C> {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.impl_
            }
        }

        /*
        impl<'a, C: Component> join::Join for &'a $wrapper<C>
            where &'a C::$impl_assoc: join::Join
        {
            type Type = <&'a C::$impl_assoc as join::Join>::Type;
            type Value = <&'a C::$impl_assoc as join::Join>::Value;
            type Mask = <&'a C::$impl_assoc as join::Join>::Mask;

            fn open(self) -> (Self::Mask, Self::Value){ self.impl_.open() }
            unsafe fn get(val: &mut Self::Value, idx: u32) -> Self::Type { <&C::$impl_assoc as join::Join>::get(val, idx) }
        }
        */
    }
}

impl_wrapper!(StateStore, StateStoreImpl);
impl_wrapper!(DiffStore, DiffStoreImpl);

impl<C: Component> Blend<StateStore<C>> for DiffStore<C> {
    fn blend_to(&mut self, dest: &mut StateStore<C>){ self.impl_.blend_to(&mut dest.impl_) }
}

impl<C: Component> Instantiate<StateStore<C>> for DiffStore<C> {
    /* */
}
