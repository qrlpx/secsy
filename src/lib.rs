#![feature(associated_type_defaults)]
#![feature(pub_restricted)]
#![feature(box_syntax)]
#![feature(integer_atomics)]

extern crate ioc;
#[macro_use] extern crate downcast;
extern crate vec_map;
extern crate tuple_utils;
extern crate rayon;
extern crate crossbeam;

///NOTE: this will eventually be moved into its own crate
pub mod bitset;
///NOTE: this will eventually be moved into its own crate
pub mod join;

// ++++++++++++++++++++ core ++++++++++++++++++++

pub mod world;
pub mod external;
pub mod pipeline;

pub use world::{Index, Id, Value, StoreBase, Store, Head, Entry, World, WorldBuilder};
pub use world::ioc_ext;
pub use external::{SubsystemBase, External, ExternalBuilder};
pub use pipeline::{ProcessBase, Process, Pipeline, StageBuilder, PipelineBuilder};

// ++++++++++++++++++++ reference impls ++++++++++++++++++++

pub mod null_store;
pub mod vec_store;
pub mod btree_map_store;

pub mod safe_index;

pub use null_store::NullStore;
pub use vec_store::VecStore;
pub use btree_map_store::BTreeMapStore;

pub use safe_index::SafeIndex;

// ++++++++++++++++++++ util ++++++++++++++++++++

/// Shorthand for `join::JoinIter::new(join)`
pub fn join<J>(join: J) -> join::JoinIter<J> 
    where J: join::Join
{
    join::JoinIter::new(join)
}

/// Shorthand for `world::entry::GetTuple::get(args, idx)`
pub fn get<Args>(args: Args, idx: Index) -> Option<Args::Refs>
    where Args: world::entry::GetTuple
{
    args.get(idx)
}

/// Shorthand for `world::entry::GetTuple::get_opt(args, idx)`
pub fn get_opt<Args>(args: Args, idx: Index) -> Args::RefOpts
    where Args: world::entry::GetTuple
{
    args.get_opt(idx)
}

/// Shorthand for `world::entry::InsertTuple::insert(args, idx, val)`
pub fn insert<Args>(args: Args, idx: Index, vals: Args::Vals) -> Args::ValOpts
    where Args: world::entry::InsertTuple
{
    args.insert(idx, vals)
}

/// Shorthand for `world::entry::InsertTuple::remove(args, idx)`
pub fn remove<Args>(args: Args, idx: Index) -> Option<Args::Vals>
    where Args: world::entry::InsertTuple
{
    args.remove(idx)
}

/// Shorthand for `world::entry::InsertTuple::remove_opt(args, idx)`
pub fn remove_opt<Args>(args: Args, idx: Index) -> Args::ValOpts
    where Args: world::entry::InsertTuple
{
    args.remove_opt(idx)
}

