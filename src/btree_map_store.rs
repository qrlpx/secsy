use world::{Index, Value, StoreBase, Store, Instantiate, Blend};
use bitset::{BitSet, BitSetLike};
use join::Join;

use std::collections::BTreeMap;
use std::mem;

// ++++++++++++++++++++ BTreeMapStore ++++++++++++++++++++

pub struct BTreeMapStore<V: Value> {
    data: BTreeMap<Index, V>,
    mask: BitSet,
}

impl<V: Value> BTreeMapStore<V> {
    pub fn new() -> Self {
        BTreeMapStore{ data: BTreeMap::new(), mask: BitSet::new() }
    }
}

impl<V: Value> StoreBase for BTreeMapStore<V> {
    fn contains(&self, idx: Index) -> bool {
        self.mask.contains(idx)
    }

    fn delete(&mut self, deletions: &BitSet){
        for idx in BitSetLike::iter(deletions) {
            self.remove(idx);
        }
    }
    fn clear(&mut self){ 
        self.data.clear();
        self.mask.clear(); 
    }
}

impl<V: Value> Store for BTreeMapStore<V> {
    type Value = V;

    fn get(&self, idx: Index) -> Option<&Self::Value> {
        if self.mask.contains(idx) {
           self.data.get(&idx)
        } else {
            None
        }
    }
    fn get_mut(&mut self, idx: Index) -> Option<&mut Self::Value> {
        if self.mask.contains(idx) {
            self.data.get_mut(&idx)
        } else {
            None
        }
    }

    fn insert(&mut self, idx: Index, val: Self::Value) -> Option<Self::Value> {
        self.mask.insert(idx);
        self.data.insert(idx, val)
    }

    fn remove(&mut self, idx: Index) -> Option<Self::Value> {
        if self.mask.remove(idx) {
            self.data.remove(&idx)
        } else {
            None
        }
    }
}
    
impl<V, Src> Instantiate<Src> for BTreeMapStore<V>
    where V: Value, Src: Store<Value = V>
{}

impl<V, Dest> Blend<Dest> for BTreeMapStore<V>
    where V: Value, Dest: Store<Value = V>
{
    fn blend_to(&mut self, dest: &mut Dest){
        for (idx, val) in mem::replace(&mut self.data, BTreeMap::new()) {
            dest.insert(idx, val);
        }
        self.mask.clear();
    }
}

impl<'a, V: Value> Join for &'a BTreeMapStore<V> {
    type Type = &'a V;
    type Value = &'a BTreeMap<Index, V>;
    type Mask = &'a BitSet;

    fn open(self) -> (Self::Mask, Self::Value) {
        (&self.mask, &self.data)
    }

    unsafe fn get(data: &mut Self::Value, idx: u32) -> Self::Type {
        &data[&idx]
    }
}

impl<V: Value> Default for BTreeMapStore<V> {
    fn default() -> Self { Self::new() }
}



