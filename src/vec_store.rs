use world::{Index, Value, StoreBase, Store, Instantiate, Blend};
use bitset::{BitSet, BitSetLike};
use join::Join;

use std::{ptr, mem};

// ++++++++++++++++++++ VecStore ++++++++++++++++++++

pub struct InnerVec<V>(Vec<V>);

pub struct VecStore<V: Value> {
    data: InnerVec<V>,
    mask: BitSet,
}

impl<V: Value> Drop for VecStore<V> {
    fn drop(&mut self){ self.clear(); }
}

impl<V: Value> VecStore<V> {
    pub fn new() -> Self {
        VecStore{ data: InnerVec(Vec::new()), mask: BitSet::new() }
    }
}

impl<V: Value> StoreBase for VecStore<V> {
    fn contains(&self, idx: Index) -> bool {
        self.mask.contains(idx)
    }

    fn delete(&mut self, deletions: &BitSet){
        for idx in BitSetLike::iter(deletions) {
            self.remove(idx);
        }
    }
    fn clear(&mut self){ 
        for idx in BitSetLike::iter(&self.mask) {
            unsafe { ptr::read(&self.data.0[idx as usize]); }
        }
        unsafe { self.data.0.set_len(0); }
        self.mask.clear(); 
    }
}

impl<V: Value> Store for VecStore<V> {
    type Value = V;

    fn get(&self, idx: Index) -> Option<&Self::Value> {
        if self.mask.contains(idx) {
            Some(&self.data.0[idx as usize])
        } else {
            None
        }
    }
    fn get_mut(&mut self, idx: Index) -> Option<&mut Self::Value> {
        if self.mask.contains(idx) {
            Some(&mut self.data.0[idx as usize])
        } else {
            None
        }
    }

    fn insert(&mut self, idx: Index, val: Self::Value) -> Option<Self::Value> {
        if self.mask.insert(idx) { //NOTE BitSet returns true when value already existed
            Some(mem::replace(&mut self.data.0[idx as usize], val))
        } else {
            let len = self.data.0.len();
            if idx as usize >= len {
                self.data.0.extend((0..idx as usize + 1 - len).map(
                    |_| unsafe { mem::uninitialized() }
                ))
            }
            unsafe { ptr::write(&mut self.data.0[idx as usize], val); }
            None
        }
    }

    fn remove(&mut self, idx: Index) -> Option<Self::Value> {
        if self.mask.remove(idx) {
            unsafe { Some(ptr::read(&self.data.0[idx as usize])) }
        } else {
            None
        }
    }
}

impl<V, Src> Instantiate<Src> for VecStore<V>
    where V: Value, Src: Store<Value = V>
{}

impl<V, Dest> Blend<Dest> for VecStore<V>
    where V: Value, Dest: Store<Value = V>
{
    fn blend_to(&mut self, dest: &mut Dest){
        for (idx, val) in Join::iter(&*self) {
            dest.insert(idx as Index, unsafe { ptr::read(val) });
        }
        unsafe { self.data.0.set_len(0); }
        self.mask.clear();
    }
}

impl<'a, V: Value> Join for &'a VecStore<V> {
    type Type = &'a V;
    type Value = &'a InnerVec<V>;
    type Mask = &'a BitSet;

    fn open(self) -> (Self::Mask, Self::Value) {
        (&self.mask, &self.data)
    }

    unsafe fn get(data: &mut Self::Value, idx: u32) -> Self::Type {
        &data.0[idx as usize]
    }
}

impl<V: Value> Default for VecStore<V> {
    fn default() -> Self { Self::new() }
}



