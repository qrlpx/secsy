use super::world::{StoreBase, Id, Head, World, WorldBuilder};
use super::world::ioc_ext;
use super::external::{SubsystemBase, External, ExternalBuilder};

use std::collections::BTreeMap;

use ioc;

// ++++++++++++++++++++ Process ++++++++++++++++++++

///TODO naming?
pub trait ProcessBase<
    ID, 
    Key = String, 
    StoBase: ?Sized = StoreBase, 
    SsysBase: ?Sized = SubsystemBase
>: Send + Sync + 'static
    where ID: Id, StoBase: StoreBase, SsysBase: SubsystemBase
{
    fn process(
        &mut self, 
        world: &World<ID, Key, StoBase>,
        external: &External<Key, SsysBase>,
    );
}

pub trait Process<'a, 
    ID, 
    Key = String, 
    StoBase: ?Sized = StoreBase, 
    SsysBase: ?Sized = SubsystemBase
>: Send + Sync + 'static
where //TODO get rid of some of those bounds
    ID: Id,
    Key: ioc::Key,
    StoBase: StoreBase,
    SsysBase: SubsystemBase,
    ioc_ext::ReadState<<Self as Process<'a, ID, Key, StoBase, SsysBase>>::State>: ioc::Method<'a, Key, StoBase>,
    ioc_ext::ReadDiff<<Self as Process<'a, ID, Key, StoBase, SsysBase>>::DiffIn>: ioc::Method<'a, Key, StoBase>,
    ioc_ext::WriteDiff<<Self as Process<'a, ID, Key, StoBase, SsysBase>>::DiffOut>: ioc::Method<'a, Key, StoBase>,
{
    type State: ioc_ext::ComponentTuple;
    type DiffIn: ioc_ext::ComponentTuple;
    type DiffOut: ioc_ext::ComponentTuple;
    type External: ioc::Method<'a, Key, SsysBase>;

    type StateRet = <ioc_ext::ReadState<Self::State> as ioc::Method<'a, Key, StoBase>>::Ret;
    type DiffInRet = <ioc_ext::ReadDiff<Self::DiffIn> as ioc::Method<'a, Key, StoBase>>::Ret;
    type DiffOutRet = <ioc_ext::WriteDiff<Self::DiffOut> as ioc::Method<'a, Key, StoBase>>::Ret;
    type ExternalRet = <Self::External as ioc::Method<'a, Key, SsysBase>>::Ret;

    fn process(
        &mut self,
        head: &Head<ID>,
        state: <ioc_ext::ReadState<Self::State> as ioc::Method<'a, Key, StoBase>>::Ret,
        diff_in: <ioc_ext::ReadDiff<Self::DiffIn> as ioc::Method<'a, Key, StoBase>>::Ret,
        diff_out: <ioc_ext::WriteDiff<Self::DiffOut> as ioc::Method<'a, Key, StoBase>>::Ret,
        external: <Self::External as ioc::Method<'a, Key, SsysBase>>::Ret,
    );
}

///Implement `ProcessBase` for `T: Process`. This is a workaround and will be eventually replaced
///by a properly working default-impl.
#[macro_export]
macro_rules! secsy_autoimpl_process_base {
    ($idgen:ty, $key:ty, $stobase:ty, $ssysbase:ty, $prok:ty) => {
        impl ::secsy::ProcessBase<$idgen, $key, $stobase, $ssysbase> for $prok { 
            fn process(
                &mut self, 
                world: &::secsy::World<$idgen, $key, $stobase>,
                external: &::secsy::External<$key, $ssysbase>,
            ){
                let state = world.state().resolve::<$crate::ioc_ext::ReadState<<$prok as ::secsy::Process<_, _, _, _>>::State>>().unwrap();
                let diff_in = world.diff().resolve::<$crate::ioc_ext::ReadDiff<<$prok as ::secsy::Process<_, _, _, _>>::DiffIn>>().unwrap();
                let diff_out = world.diff().resolve::<$crate::ioc_ext::WriteDiff<<$prok as ::secsy::Process<_, _, _, _>>::DiffOut>>().unwrap();
                let external = external.resolve::<<$prok as ::secsy::Process<_, _, _, _>>::External>().unwrap();
                <$prok as ::secsy::Process<_, _, _, _>>::process(self, world.head(), state, diff_in, diff_out, external);
            }
        }
    };
}

impl<ID, Key, StoBase: ?Sized, SsysBase: ?Sized, T> ProcessBase<ID, Key, StoBase, SsysBase> for T
where //TODO get rid of some of those bounds
    T: for<'a> Process<'a, ID, Key, StoBase, SsysBase>,
    ID: Id,
    Key: ioc::Key,
    StoBase: StoreBase,
    SsysBase: SubsystemBase,
    for<'a> ioc_ext::ReadState<<T as Process<'a, ID, Key, StoBase, SsysBase>>::State>: ioc::Method<'a, Key, StoBase>,
    for<'a> ioc_ext::ReadDiff<<T as Process<'a, ID, Key, StoBase, SsysBase>>::DiffIn>: ioc::Method<'a, Key, StoBase>,
    for<'a> ioc_ext::WriteDiff<<T as Process<'a, ID, Key, StoBase, SsysBase>>::DiffOut>: ioc::Method<'a, Key, StoBase>,
{
    fn process(
        &mut self, 
        world: &World<ID, Key, StoBase>,
        external: &External<Key, SsysBase>,
    ){
        let state = world.state().resolve::<ioc_ext::ReadState<T::State>>().unwrap();
        let diff_in = world.diff().resolve::<ioc_ext::ReadDiff<T::DiffIn>>().unwrap();
        let diff_out = world.diff().resolve::<ioc_ext::WriteDiff<T::DiffOut>>().unwrap();
        let external = external.resolve::<T::External>().unwrap();
        <T as Process<_, _, _, _>>::process(self, world.head(), state, diff_in, diff_out, external);
    }
}


// ++++++++++++++++++++ Pipeline ++++++++++++++++++++

pub struct Pipeline<
    ID,
    Key = String,
    StoBase: ?Sized = StoreBase,
    SsysBase: ?Sized = SubsystemBase
>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase, SsysBase: SubsystemBase
{
    stages: BTreeMap<String, Vec<Box<ProcessBase<ID, Key, StoBase, SsysBase>>>>,
    world: World<ID, Key, StoBase>,
    external: External<Key, SsysBase>
}

impl<ID, Key, StoBase: ?Sized, SsysBase: ?Sized> Pipeline<ID, Key, StoBase, SsysBase>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase, SsysBase: SubsystemBase
{
    pub fn world(&self) -> &World<ID, Key, StoBase> { &self.world }
    pub fn world_mut(&mut self) -> &mut World<ID, Key, StoBase> { &mut self.world }
    pub fn external(&self) -> &External<Key, SsysBase> { &self.external }

    pub fn reset(&mut self){
        use rayon::prelude::*;
        use rayon;

        let external: Vec<_> = self.external.services().iter().collect();
        let world = &mut self.world;
        rayon::join(|| {
            /* trigger on_reset */
            external.into_par_iter().weight_max().for_each(|(_, ssys)| {
                ssys.write().unwrap().on_reset()
            });  
        }, || {
            world.clear();
        });
    }

    pub fn next_frame(&mut self){
        use rayon::prelude::*;
        use rayon;

        /* trigger on_before_frame */
        {
            let external: Vec<_> = self.external.services().iter().collect();
            external.into_par_iter().weight_max().for_each(|(_, ssys)| {
                ssys.write().unwrap().on_before_frame()
            });
        }

        /* step through stages */
        for (_, stage) in &mut self.stages {
            {
                let (world, external) = (&self.world, &self.external);
                stage.par_iter_mut().weight_max().for_each(|prok| prok.process(world, external));
            }
            self.world.flush_stage();
        }

        let external: Vec<_> = self.external.services().iter().collect();
        let world = &mut self.world;
        rayon::join(|| {
            /* trigger on_after_frame */
            external.into_par_iter().weight_max().for_each(|(_, ssys)| {
                ssys.write().unwrap().on_after_frame()
            });
        }, || {
            world.flush_frame();  
        });
    }
}

// ++++++++++++++++++++ PipelineBuilder ++++++++++++++++++++

pub struct StageBuilder<'a, 
    ID, 
    Key = String, 
    StoBase: ?Sized = StoreBase, 
    SsysBase: ?Sized = SubsystemBase
> 
    where ID: Id, Key: ioc::Key, StoBase: StoreBase, SsysBase: SubsystemBase
{
    processes: &'a mut Vec<Box<ProcessBase<ID, Key, StoBase, SsysBase>>>
}

impl<'a, ID, Key, StoBase: ?Sized, SsysBase: ?Sized> StageBuilder<'a, ID, Key, StoBase, SsysBase>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase, SsysBase: SubsystemBase
{
    pub fn add_process(
        &mut self,
        prok: Box<ProcessBase<ID, Key, StoBase, SsysBase>>
    ) -> &mut Self {
        self.processes.push(prok);
        self
    }
    
    pub fn add<P>(&mut self, prok: P) -> &mut Self
        where P: ProcessBase<ID, Key, StoBase, SsysBase>
    {
        self.add_process(Box::new(prok))
    }
    
    pub fn add_default<P>(&mut self) -> &mut Self
        where P: Default + ProcessBase<ID, Key, StoBase, SsysBase>
    {
        self.add(P::default())
    }
}

pub struct PipelineBuilder<
    ID, 
    Key = String, 
    StoBase: ?Sized = StoreBase, 
    SsysBase: ?Sized = SubsystemBase
> 
    where ID: Id, Key: ioc::Key, StoBase: StoreBase, SsysBase: SubsystemBase
{
    stages: BTreeMap<String, Vec<Box<ProcessBase<ID, Key, StoBase, SsysBase>>>>,
    world: WorldBuilder<ID, Key, StoBase>,
    external: ExternalBuilder<Key, SsysBase>
}

impl<ID, Key, StoBase: ?Sized, SsysBase: ?Sized> PipelineBuilder<ID, Key, StoBase, SsysBase>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase, SsysBase: SubsystemBase
{
    pub fn new() -> Self {
        PipelineBuilder{ 
            stages: BTreeMap::new(),
            world: WorldBuilder::new(),
            external: ExternalBuilder::new()
        }
    }

    pub fn stage<S>(&mut self, name: S) -> StageBuilder<ID, Key, StoBase, SsysBase>
        where S: Into<String>
    {
        StageBuilder{ processes: self.stages.entry(name.into()).or_insert(Vec::new()) }
    }

    pub fn world(&mut self) -> &mut WorldBuilder<ID, Key, StoBase> { &mut self.world }
    pub fn external(&mut self) -> &mut ExternalBuilder<Key, SsysBase> { &mut self.external }

    pub fn build(self) -> Pipeline<ID, Key, StoBase, SsysBase> {
        Pipeline{ 
            stages: self.stages, 
            world: self.world.build(), 
            external: self.external.build()
        }
    }
}

impl<ID, Key, StoBase: ?Sized, SsysBase: ?Sized> Default for PipelineBuilder<ID, Key, StoBase, SsysBase>
    where ID: Id, Key: ioc::Key, StoBase: StoreBase, SsysBase: SubsystemBase
{
    fn default() -> Self { Self::new() }
}

