use world::{Index, Value, StoreBase, Store};
use bitset::BitSet;

use std::marker::PhantomData;

pub struct NullStore<V>(PhantomData<V>);

impl<V: Value> Default for NullStore<V> {
    fn default() -> Self { NullStore(PhantomData) }
}

impl<V: Value> StoreBase for NullStore<V> {
    fn contains(&self, _idx: Index) -> bool { false }

    fn delete(&mut self, _: &BitSet){}
    fn clear(&mut self){}
}

impl<V: Value> Store for NullStore<V> {
    type Value = V;

    fn get(&self, _idx: Index) -> Option<&Self::Value> { None }
    fn get_mut(&mut self, _idx: Index) -> Option<&mut Self::Value> { None }

    fn insert(&mut self, _: Index, _: Self::Value) -> Option<Self::Value> { None }

    fn remove(&mut self, _: Index) -> Option<Self::Value> { None }
}



