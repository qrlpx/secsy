use world::{Index, Generation, Id};

// ++++++++++++++++++++ SafeIndex ++++++++++++++++++++

#[derive(Hash, Debug, Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct SafeIndex {
    idx: u32,
    gen: u8,
}

impl Id for SafeIndex {
    type UserData = ();

    fn new_unchecked(idx: Index, gen: Generation, user_data: Self::UserData) -> Self {
        SafeIndex{ idx: idx, gen: gen as u8 }
    }

    fn max_index() -> Index { (!0u32) as Index }
    fn max_generation() -> Generation { (!0u8) as Index }

    fn index(self) -> Index { self.idx }
    fn generation(self) -> Generation { self.gen as Generation }
    fn user_data(self) -> Self::UserData { () }
}

